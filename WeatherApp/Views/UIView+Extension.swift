//
//  UIView+Extension.swift
//  WeatherApp
//
//  Created by Muhammed on 10/29/20.
//  Copyright © 2020 Muhammmed. All rights reserved.
//

import UIKit
extension UIView {
    class func fromNib(named: String? = nil) -> Self {
        let name = named ?? "\(Self.self)"
        guard
            let nib = Bundle.main.loadNibNamed(name, owner: nil, options: nil)
            else { fatalError("missing expected nib named: \(name)") }
        guard
            let view = nib.first as? Self
            else { fatalError("view of type \(Self.self) not found in \(nib)") }
        return view
    }
}
