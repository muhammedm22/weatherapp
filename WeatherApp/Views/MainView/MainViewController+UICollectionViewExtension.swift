//
//  MainViewController+UICollectionViewExtension.swift
//  WeatherApp
//
//  Created by Muhammed on 10/30/20.
//  Copyright © 2020 Muhammmed. All rights reserved.
//

import UIKit
// MARK: - CollectionView
extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = citiesCollectionView.dequeueReusableCell(withReuseIdentifier: "CityCollectionViewCell", for: indexPath) as! CityCollectionViewCell
        let forecast = viewModel.selectedForecasts[indexPath.row]
        cell.removeButtonAction = { [unowned self] in
            self.viewModel.removeCityFromActiviyAt(index:indexPath.row)
            self.citiesCollectionView.reloadData()
        }
        cell.setupCityWith(forecast: forecast)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.selectedForecasts.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 12, height: 200)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.citiesCollectionView.deselectItem(at: indexPath, animated: false)
    }
}
