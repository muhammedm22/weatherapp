//
//  ViewController.swift
//  WeatherApp
//
//  Created by Muhammed on 10/29/20.
//  Copyright © 2020 Muhammmed. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var resultsTableView: UITableView!
    @IBOutlet weak var citiesCollectionView: UICollectionView!
    @IBOutlet private var activityIndicator: UIActivityIndicatorView!
    // MARK: - Properties
    let searchResultTableViewCellId = "SearchTableViewCell"
    let citiesCollectionViewCellId = "CityCollectionViewCell"
    var viewModel:MainViewModel!
    var searchCities = [CityModel]()
    var selectedCitites = [Forecast]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupTable()
        setupViewModel()
        setupSearchBar()
        setupCollectionView()
        setupLoader()
    }
    
    func setupViewModel() {
        self.viewModel = MainViewModel()
        self.viewModel.getAllCities(completion: { [unowned self] result in
            switch result {
            case .success():
                self.resultsTableView.reloadData()
            case .failure(let error):
                print("Didnt get cities with error", error)
            }
        })
        
        self.viewModel.getForecasts(completion: { [unowned self] result in
            switch result {
            case .success():
                self.stopLoader()
                self.citiesCollectionView.reloadData()
            case .failure(let error):
                print("Didnt get cities with error", error)
            }
        })
    }
    
    func setupTable() {
        self.resultsTableView.delegate = self
        self.resultsTableView.dataSource = self
        self.resultsTableView.register(SearchTableViewCell.self, forCellReuseIdentifier: searchResultTableViewCellId)
        self.resultsTableView.isHidden = true
    }
    
    func setupCollectionView() {
        self.citiesCollectionView.delegate = self
        self.citiesCollectionView.dataSource = self
        self.citiesCollectionView.register(UINib(nibName: citiesCollectionViewCellId, bundle: nil), forCellWithReuseIdentifier: citiesCollectionViewCellId)

    }
    
    func setupSearchBar() {
        self.searchBar.delegate = self
        self.searchBar.placeholder = kSearchBarPlaceholder
    }
    
    func setupLoader() {
        self.activityIndicator.startAnimating()
    }
    
    func stopLoader() {
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
    }
    
    func showAlerOfMaximum() {
        let alert = UIAlertController(title: "Alert", message: "Maximum of activities is \(viewModel.maximumCitiesNumber)", preferredStyle: .alert)
        let action = UIAlertAction(title: "ok", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
}
// MARK: - TableView
extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = resultsTableView.dequeueReusableCell(withIdentifier: searchResultTableViewCellId) as! SearchTableViewCell
        cell.textLabel?.text = searchCities[indexPath.row].city.name
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchCities.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        toggleTableView(hiddenTableView: true)
        searchBar.text = ""
        let cityID = searchCities[indexPath.row].city.id.numberLong
        self.viewModel.selectCityWith(cityID: cityID, completion: { [unowned self] result in
            switch result {
            case .success(let forecast):
                if self.viewModel.selectedForecasts.count == self.viewModel.maximumCitiesNumber {
                    self.showAlerOfMaximum()
                    return
                }
                self.viewModel.addCityToActivity(forecast: forecast)
                self.citiesCollectionView.reloadData()
            case .failure(let error):
                print("couldn't get data with error",error)
            }
        })
    }
}
// MARK: - Searchbar
extension MainViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchCities = viewModel.cities.filter({ $0.city.name.contains(searchText)})
        self.resultsTableView.reloadData()
        self.toggleTableView(hiddenTableView: searchText.count > 0 ? false : true)
    }
    func toggleTableView(hiddenTableView:Bool) {
        self.resultsTableView.isHidden = hiddenTableView
    }
}
