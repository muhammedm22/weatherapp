//
//  CityCollectionViewCell.swift
//  WeatherApp
//
//  Created by Muhammed on 10/30/20.
//  Copyright © 2020 Muhammmed. All rights reserved.
//

import UIKit

class CityCollectionViewCell: UICollectionViewCell {
    //MARK: - Outlets
    @IBOutlet private weak var cityName: UILabel!
    @IBOutlet private weak var temp: UILabel!
    @IBOutlet private weak var datesStackView: UIStackView!
    @IBOutlet private weak var tempsStackView: UIStackView!
    // MARK: - Properties
    var removeButtonAction:(() -> ())?
    // MARK: - Mehtods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setupCityWith(forecast: Forecast) {
        self.cityName.text = forecast.city.name
        if let temp = forecast.list.first?.main.temp {
            self.temp.text = "\(temp) c"
        }
        setDateTempForDays(forecast: forecast)
    }
    
    func setDateTempForDays(forecast: Forecast) {
        let nextDays = forecast.list.filter({ forecast in
            return forecast.dtTxt.contains(kFirstHourStringFormat)
        })
        if datesStackView.arrangedSubviews.count == 0 {
            for nextDay in nextDays {
                let dateView = UILabel()
                dateView.font = UIFont(name: "Arial", size: 14)
                let date = Date(timeIntervalSince1970: Double(nextDay.dt))
                let calendar = Calendar.current
                let day = calendar.component(.weekday, from: date)
                dateView.text = convertDayUnitToName(unit: day)
                self.datesStackView.addArrangedSubview(dateView)
                
                let tempView = UILabel()
                tempView.text = "\(nextDay.main.temp) c"
                self.tempsStackView.addArrangedSubview(tempView)
            }
        }
    }
    
// MARK: - Action
    @IBAction func removeCity(_ sender: UIButton) {
        self.removeButtonAction?()
    }
}
