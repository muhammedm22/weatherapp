//
//  SearchResultsViewModel.swift
//  WeatherApp
//
//  Created by Muhammed on 10/29/20.
//  Copyright © 2020 Muhammmed. All rights reserved.
//

import UIKit
import CoreLocation

class MainViewModel {
    // MARK: - Properties
    var cities = [CityModel]()
    var selectedForecasts = [Forecast]()
    var maximumCitiesNumber = 5
    var userCurrentLocation: CLLocation?
    var forecastLocalStore = ForecastLocalStore()
    // MARK: - Methods
    init() {
        setupUserLocation()
    }
    func setupUserLocation() {
        let location = LocationManager.shared
        location.delegate = self
    }
    
    func setDefaultCity(completion: @escaping (Result<Void,Error>) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0 , execute: { [unowned self] in
            var lat = kDefaultCityLat
            var long = kDefaultCityLong
            if LocationManager.shared.checkIfLocationServicesEnabled() {
                lat = "\(self.userCurrentLocation?.coordinate.latitude ?? 0.0)"
                long = "\(self.userCurrentLocation?.coordinate.longitude ?? 0.0)"
            }
            APIClient.getForcastsWith(lat: lat, long: long, completion: { result in
                switch result {
                case .success(let forecast):
                    self.selectedForecasts.append(forecast)
                    completion(.success(()))
                case .failure(let error):
                    completion(.failure(error))
                }
            })
        })
    }
    
    func getLocalData(completion: @escaping (Result<Void,Error>) -> Void) {
        self.selectedForecasts = forecastLocalStore.getForecasts()
    }
    
    func getForecasts(completion: @escaping (Result<Void,Error>) -> Void) {
        if Reachability.isConnectedToNetwork() {
            self.setDefaultCity(completion: completion)
        } else {
            self.getLocalData(completion: completion)
        }
    }
    
    func getAllCities(completion: @escaping (Result<Void,Error>) -> Void) {
        guard let url = Bundle.main.url(forResource: kCitiesFileName, withExtension: "json") else { return }
        do {
            let data = try Data(contentsOf: url)
            let decoder = JSONDecoder()
            let jsonData = try decoder.decode([CityModel].self, from: data)
            self.cities = jsonData
            completion(.success(()))
        } catch {
            print("Couldn't get data from json with error", error)
            completion(.failure(error))
        }
    }
    func selectCityWith(cityID: String, completion: @escaping (Result<Forecast,Error>) -> Void) {
        APIClient.getForcastsWith(cityID: cityID, completion: { result in
            switch result {
            case .success(let forecast):
                completion(.success(forecast))
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
    func addCityToActivity(forecast: Forecast) {
            if selectedForecasts.count < maximumCitiesNumber {
                self.selectedForecasts.append(forecast)
            }
            if forecastLocalStore.getForecasts().count < maximumCitiesNumber {
                self.forecastLocalStore.addForecast(forecast: forecast)
            }
    }
    func removeCityFromActiviyAt(index: Int) {
        self.selectedForecasts.remove(at: index)
    }
}
extension MainViewModel: LocationUpdateProtocol {
    func locationDidUpdateToLocation(location: CLLocation) {
        self.userCurrentLocation = location
    }
}
