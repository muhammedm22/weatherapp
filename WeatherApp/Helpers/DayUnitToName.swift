//
//  DayUnitToName.swift
//  WeatherApp
//
//  Created by Muhammed on 10/31/20.
//  Copyright © 2020 Muhammmed. All rights reserved.
//

import Foundation

func convertDayUnitToName(unit: Int) -> String {
    switch unit {
    case 1:
        return "Sunday"
    case 2:
        return "Monday"
    case 3:
        return "Tuesday"
    case 4:
        return "Wendsday"
    case 5:
        return "Thrusday"
    case 6:
        return "Friday"
    case 7:
        return "Saturday"
    default:
        return ""
    }
}
