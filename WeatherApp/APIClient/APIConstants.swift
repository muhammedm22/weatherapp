//
//  APIConstants.swift
//  WeatherApp
//
//  Created by Muhammed on 10/30/20.
//  Copyright © 2020 Muhammmed. All rights reserved.
//

import Foundation

struct APIConstants {
    let baseURL = "http://api.openweathermap.org"
    let appID = "8fb8317e07f323b5c15684df84295b68"
    
    enum HTTPHeaderKeyField: String {
        case contentType = "Content-Type"
        case acceptType = "Accept"
    }

    enum HTTPHeaderValueField: String {
        case applicationJson = "application/json"
    }
}


