//
//  APIRouter.swift
//  WeatherApp
//
//  Created by Muhammed on 10/29/20.
//  Copyright © 2020 Muhammmed. All rights reserved.
//
import Alamofire

enum APIRouter: URLRequestConvertible {

    case forcasts(parameters: Parameters)

    // MARK: - HTTPMethod
    private var method: HTTPMethod {
        return .get
    }

    // MARK: - Path
    private var path: String {
        switch self {
        default:
            return "/data/2.5/forecast"
        }
    }

    // MARK: - Parameters
    private var parameters: Parameters? {
        switch self {
        case .forcasts(let parameters):
            return parameters
        }
    }

    private var body: Parameters? {
        switch self {
        default:
            return nil
        }

    }
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try APIConstants().baseURL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        // Common Headers
        urlRequest.setValue(APIConstants.HTTPHeaderValueField.applicationJson.rawValue, forHTTPHeaderField: APIConstants.HTTPHeaderKeyField.acceptType.rawValue)
        urlRequest.setValue(APIConstants.HTTPHeaderValueField.applicationJson.rawValue, forHTTPHeaderField: APIConstants.HTTPHeaderKeyField.contentType.rawValue)

        // Add http body to request
        if let body = body {
            do {
                let data = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
                urlRequest.httpBody = data
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
        // Add query parameters to request
        if let parameters = parameters {
            urlRequest = try URLEncoding.queryString.encode(urlRequest, with: parameters)
        }

        return urlRequest
    }
}


