//
//  APIClient.swift
//  WeatherApp
//
//  Created by Muhammed on 10/29/20.
//  Copyright © 2020 Muhammmed. All rights reserved.
//

import Alamofire

class APIClient {
    private static func performRequest<T: Decodable>(route: APIRouter, decoder: JSONDecoder = JSONDecoder(), completion:@escaping (Result<T, AFError>)->Void) -> DataRequest {
        return AF.request(route)
                        .responseDecodable (decoder: decoder){ (response: DataResponse<T, AFError>) in
                            completion(response.result)
        }
    }
    
    static func getForcastsWith(cityID: String, completion: @escaping (Result<Forecast, AFError>)-> Void) {
        let parameters = [
            "id": cityID,
            "APPID": APIConstants().appID,
            "units": kDefaultTempUnit,
            ]
       _ = performRequest(route: APIRouter.forcasts(parameters: parameters), completion: completion)
    }
    static func getForcastsWith(lat: String, long: String, completion: @escaping (Result<Forecast, AFError>)-> Void) {
        let parameters = [
            "lat": lat,
            "lon": long,
            "APPID": APIConstants().appID,
            "units": kDefaultTempUnit,
            ]
       _ = performRequest(route: APIRouter.forcasts(parameters: parameters), completion: completion)
    }
}
