//
//  LocationManager.swift
//  WeatherApp
//
//  Created by Muhammed on 10/31/20.
//  Copyright © 2020 Muhammmed. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocationUpdateProtocol {
    func locationDidUpdateToLocation(location : CLLocation)
}

class LocationManager: NSObject, CLLocationManagerDelegate {
    // MARK: - Properties
    static let shared = LocationManager()
    private var locationManager = CLLocationManager()
    var currentCoordinate: CLLocation!
    var delegate: LocationUpdateProtocol!
    // MARK: - Methods
    override init() {
        super.init()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    func checkIfLocationServicesEnabled() -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                return false
            case .authorizedAlways, .authorizedWhenInUse:
                return true
            @unknown default:
                return false
            }
        } else {
            return false
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentCoordinate = locations.first
        DispatchQueue.main.async(execute: {
            self.delegate.locationDidUpdateToLocation(location: self.currentCoordinate)
        })
    }
}
