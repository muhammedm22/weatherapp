//
//  Constants.swift
//  WeatherApp
//
//  Created by Muhammed on 10/29/20.
//  Copyright © 2020 Muhammmed. All rights reserved.
//

import Foundation


let kCitiesFileName = "Cities"
let kDefaultTempUnit = "metric"
let kDefaultCityLat = "51.509865"
let kDefaultCityLong = "-0.118092"

// MARK: - Messages and errors
let kSearchBarPlaceholder = "Search with city name"

// MARK: - Formats
let kFirstHourStringFormat = "00:00:00"
