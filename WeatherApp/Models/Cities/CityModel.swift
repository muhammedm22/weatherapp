//
//  Cities.swift
//  WeatherApp
//
//  Created by Muhammed on 10/29/20.
//  Copyright © 2020 Muhammmed. All rights reserved.
//

import Foundation

struct CityModel: Decodable {
    let id: IDUnion
    let city: City
}

struct IDClass: Codable {
    let numberLong: String

    enum CodingKeys: String, CodingKey {
        case numberLong = "$numberLong"
    }
}
enum IDUnion: Codable {
    case idClass(IDClass)
    case integer(Int)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Int.self) {
            self = .integer(x)
            return
        }
        if let x = try? container.decode(IDClass.self) {
            self = .idClass(x)
            return
        }
        throw DecodingError.typeMismatch(IDUnion.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for IDUnion"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .idClass(let x):
            try container.encode(x)
        case .integer(let x):
            try container.encode(x)
        }
    }
}
