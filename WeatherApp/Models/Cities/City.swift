//
//  City.swift
//  WeatherApp
//
//  Created by Muhammed on 10/29/20.
//  Copyright © 2020 Muhammmed. All rights reserved.
//

import Foundation

struct City: Decodable {
    let id: CityID
    let name: String
    let findname: String
    let country: String
}
