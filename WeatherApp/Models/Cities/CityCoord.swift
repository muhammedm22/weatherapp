//
//  CityCoord.swift
//  WeatherApp
//
//  Created by Muhammed on 10/29/20.
//  Copyright © 2020 Muhammmed. All rights reserved.
//

import Foundation

struct CityCoord: Decodable {
    let lat: Double
    let long: Double 
}
