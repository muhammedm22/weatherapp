//
//  CityID.swift
//  WeatherApp
//
//  Created by Muhammed on 10/29/20.
//  Copyright © 2020 Muhammmed. All rights reserved.
//

import Foundation

struct CityID: Decodable {
    let numberLong: String
    enum CodingKeys: String, CodingKey {
        case numberLong = "$numberLong"
    }
}
