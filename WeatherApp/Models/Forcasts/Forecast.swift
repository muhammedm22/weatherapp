//
//  Forecast.swift
//  WeatherApp
//
//  Created by Muhammed on 10/30/20.
//  Copyright © 2020 Muhammmed. All rights reserved.
//
import Foundation
import RealmSwift

class Forecast: Object, Codable {
    @objc dynamic var cod: String = ""
    @objc dynamic var message: Int = 0
    @objc dynamic var cnt: Int = 0
    let list = List<ForecastList>()
    @objc dynamic var city: ForecastCity!
    
    enum CodingKeys: String, CodingKey {
        case cod
        case message
        case cnt
        case list
        case city
    }
    
    required init(from decoder: Decoder) throws
    {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        cod = try container.decode(String.self, forKey: .cod)
        message = try container.decode(Int.self, forKey: .message)
        let lists = try container.decode([ForecastList].self, forKey: .list)
        list.append(objectsIn: lists)
        city = try container.decode(ForecastCity.self, forKey: .city)

        super.init()
    }
    
    required init()
    {
        super.init()
    }
}

// MARK: - City
class ForecastCity: Object, Codable {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var country: String = ""
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case country
    }
    
    required init(from decoder: Decoder) throws
    {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        country = try container.decode(String.self, forKey: .country)

        super.init()
    }
    
    required init()
    {
        super.init()
    }
}

// MARK: - List
class ForecastList: Object, Codable {
    @objc dynamic var dt: Int = 0
    @objc dynamic var main: ForecastMainClass!
    @objc dynamic var dtTxt: String = ""
    
    enum CodingKeys: String, CodingKey {
        case dt
        case main
        case dtTxt = "dt_txt"
    }
    required init(from decoder: Decoder) throws
    {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        dt = try container.decode(Int.self, forKey: .dt)
        main = try container.decode(ForecastMainClass.self, forKey: .main)
        dtTxt = try container.decode(String.self, forKey: .dtTxt)

        super.init()
    }
    
    required init()
    {
        super.init()
    }
}

// MARK: - MainClass
class ForecastMainClass: Object, Codable {
    @objc dynamic var temp: Double = 0.0
    @objc dynamic var tempMin: Double = 0.0
    @objc dynamic var tempMax: Double = 0.0
    
    enum CodingKeys: String, CodingKey {
        case temp
        case tempMin = "temp_min"
        case tempMax = "temp_max"
    }
    required init(from decoder: Decoder) throws
    {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        temp = try container.decode(Double.self, forKey: .temp)
        tempMin = try container.decode(Double.self, forKey: .tempMin)
        tempMax = try container.decode(Double.self, forKey: .tempMax)

        super.init()
    }
    
    required init()
    {
        super.init()
    }
}
