//
//  ForecastLocalStore.swift
//  WeatherApp
//
//  Created by Muhammed on 10/31/20.
//  Copyright © 2020 Muhammmed. All rights reserved.
//

import Foundation
import RealmSwift

class ForecastLocalStore {
    
    func addForecast(forecast: Forecast) {
         let realm = try! Realm()
         try! realm.write {
             realm.add(forecast)
        }
    }
    
    func getForecasts() -> [Forecast] {
        let realm = try! Realm()
        let forecasts = realm.objects(Forecast.self)
        return forecasts.toArray()
    }

}
extension RealmCollection {
  func toArray<T>() -> [T] {
    return self.compactMap { $0 as? T }
  }
}
